from setuptools import setup, find_packages

# -- Get required packages from requirements.txt
from os import path
setup_dir = path.dirname(path.realpath(__file__))
requirements_fp = path.sep.join([setup_dir, 'requirements.txt'])
with open(requirements_fp) as f:
    depend = f.read().splitlines()


setup(
    name='redirect_check',
    version='1.0',
    url='https://gitlab.com/mai-public/check_https_default',
    license='GNU3',
    author='MrMikeandike',
    author_email='',
    long_description='Checks to see if a website is listed as https on google, and checks if http redirects to https',
    description='Tests websites for default SSL redirect settings',

    packages=find_packages(exclude=['tests', '*_tests', '*_test', 'tests_*', 'test_*', 'requirements.txt']),
    install_requires=depend,
)
