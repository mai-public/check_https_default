# py 37

from redirect_check import url_ssl_redirect
import pytest
import os
import json

test_url = os.environ.get('TEST_URL')
test_domain = os.environ.get('TEST_DOMAIN')
results = []
@pytest.mark.parametrize("url, domain", [(test_url, test_domain)])
def test_url_ssl_redirect(url, domain):
    test_return = None
    test_return = url_ssl_redirect(url, domain)
    assert test_return is not None
    for key in ['first_search_result', "first_url_is_ssl", "redirects_http"]:
        assert bool(test_return.get(key)) is True or test_return.get(key) is False


fp = os.path.sep.join([os.environ.get('CI_PROJECT_DIR'), 'result.json'])
with open(fp, 'w') as f:
    json.dump(url_ssl_redirect(test_url, test_domain), f)
