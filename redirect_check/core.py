#!/bin/env python3

"""
EXAMPLE:
test_url = "www.example.com"
test_domain = "example.com"
result = url_ssl_redirect(test_url=test_url, test_domain=test_domain)



"""
import requests
import googlesearch
import re
import requests

def first_google_result(url, domain):
    search_params = {
        "query": url,
        "domains": [domain],
        "start": 0,
        "stop": 1,
        "pause": 2,
        "tld": "com",
        "lang": "en"


    }
    search = googlesearch.search(**search_params)
    return list(search)[0]


def get_port(url, test_url):
    # Check if url is http or https
    pattern = "".join([
        r"(?P<port>https?)",
        r"://",
        re.sub(r"\.", r"\.", test_url),
        r".*"
    ])

    match = re.match(pattern, url, re.IGNORECASE)
    assert bool(match) is True, "URL doesnt follow scheme 'http(s)://<url>*'"
    return match.groupdict()['port']

def load_url(url):
    req = requests.get(url)
    assert (200 <= req.status_code < 300), "testing redirect returned non-2xx status code"
    return req.url


def url_ssl_redirect(test_url, test_domain):

    url = first_google_result(url=test_url, domain=test_domain)
    # Check if url is http or https
    url_port = get_port(url=url, test_url=test_url)
    # get http version of url to test
    http_url = re.sub('https', 'http', url)

    after_load_url = load_url(http_url)
    after_load_url_port = get_port(url=after_load_url, test_url=test_url)
    if after_load_url_port == 'http':
        redirected = False
    elif after_load_url_port == 'https':
        redirected = True

    return {
        "first_search_result": url,
        "first_url_is_ssl": (url_port == 'https'),
        "redirects_http": redirected,

    }






